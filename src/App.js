import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const [channels, setChannels] = useState();

  useEffect(() => {
    fetch(`http://api.sr.se/api/v2/channels/?format=json`)
      .then(res => res.json())
      .then(data => {
        setChannels(data.channels);
        console.log("data: " + JSON.stringify(channels));
      });
  },[])

  return (
    <div className="App">
      <div>
        <ul>
          { channels 
            ? console.log("channels: " + JSON.stringify(channels)) 
            : null
          }
          {
            channels
            ? channels.map((channel) => {
              return(
                <li key={channel.id}>
                  <h3>{channel.name}</h3>
                  <img src={channel.image}></img>
                  <p>{channel.tagline}</p>
                  <audio
                    controls
                    src={channel.liveaudio.url}>
                        Your browser does not support the
                        <code>audio</code> element.
                </audio>
                </li>
                )
              }
            )
            : null
          }
        </ul>
      </div>
    </div>
  );
}

export default App;
